var mongoose = require('mongoose')
/* the database name is stored in a private variable instead of being 'hard-coded' so it can be replaced using the 'rewire' module. This avoids the need for the unit tests to run against the 'live' database. */
var database = 'api'
/* the server connections string includes both the database server IP address and the name of the database. */
const server = 'mongodb://'+process.env.IP+'/'+database
console.log(server)
/* the mongoose drivers connect to the server and return a connection object. */
mongoose.connect(server)
const db = mongoose.connection

/* all documents in a 'collection' must adhere to a defined 'schema'. Here we define a new schema that includes a mandatory string and an array of strings. */
const broadcastersSchema =
new mongoose.Schema({
    broadcaster: String,
    summoner: String,
    stream: {   streamAvailable:    Boolean, 
                channel:            String,
                title:              String,
                game:               String,
                viewers:            Number,
                onlineSince:        String,
                resolution:         Number,
                fps:                Number,
                delay:              Number,
                preview:            String,
                mature:             Boolean,
                language:           String,
                channelCreated:     String,
                logo:               String,
                offlineBanner:      String,
                isTwitchPartner:    Boolean,
                url:                String,
                followers:          Number
            },
    game:   {   gameAvailable:      Boolean,
                map:                Number,
                gameMode:           String,
                gameType:           String,
                teamId:             Number,
                spell1Id:           Number,
                spell2Id:           Number,
                profileIconId:      Number,
                summonerName:       String,
                summonerId:         Number,
                gameLengthSeconds:  Number
            },
})
/* the schema is associated with the 'List' collection which means it will be applied to all documents added to the collection. */
const Broadcasters = mongoose.model('Broadcasters', broadcastersSchema)
/* END OF MONGOOSE SETUP */

/* notice we are using the 'arrow function' syntax. In this example there are more than one parameter so they need to be enclosed in brackets. */
exports.addBroadcaster = (broadcast, callback) => {
  /* here we extract data from our broadcaster object, which was created using both APIs in the broadcasterPOST module*/
  const broadcaster = broadcast.broadcaster.toLowerCase()
  const summoner = broadcast.summoner
  const stream = broadcast.stream
  const game = broadcast.game
  
  /* now we have extracted the data we can use it to create a new 'List' object that adopts the correct schema. */
  const newBroadcasters = new Broadcasters({ broadcaster: broadcaster, summoner: summoner, stream: stream, game: game })
  
  newBroadcasters.save( (err, data) => {
    if (err) {
      callback('error: '+err)
    }
    callback(data)
  })
}

exports.updateBroadcaster = (broadcast, callback) => {
  /*here we extract data from our broadcaster object, which was created using both APIs in the broadcasterPOST module*/
  const broadcaster = broadcast.broadcaster.toLowerCase()
  const summoner = broadcast.summoner
  const stream = broadcast.stream
  const game = broadcast.game
  
  const update = { summoner: summoner, stream: stream, game: game }
      
      Broadcasters.update({broadcaster: broadcaster}, update, (err, data) => {
            if (err) {
                callback('error: '+err)
            }
            callback(data)
        })
}

exports.getAll = callback => {
  /* the List object contains several useful properties. The 'find' property contains a function to search the MongoDB document collection. */
  Broadcasters.find( (err, data) => {
    if (err) {
      callback('error: '+err)
    }
    const list = data.map( item => {
      return {id: item._id, broadcaster: item.broadcaster}
    })
    callback(list)
  })
}

exports.getById = (id, callback) => {
  /* the 'find' property function can take a second 'filter' parameter. */
  Broadcasters.find({_id: id}, (err, data) => {
    if (err) {
      callback('error: '+err)
    }
    callback(data)
  })
}

exports.getByBroadcaster = (broadcaster, callback) => {
  Broadcasters.find({broadcaster: broadcaster}, (err, data) => {
    if (err) {
      callback('error: '+err)
    } else if (data[0] != undefined) {
        callback({code: 200, response: {message: 'Broadcaster found.', data: data[0]}})
    } else {
        callback({code: 404, response: {message: 'Broadcaster not found.'}})
    }
  })
}

exports.deleteBroadcaster = (broadcaster, callback) => {
  Broadcasters.remove({broadcaster: broadcaster}, (err, data) => {
    if (err) {
      callback('error: '+err)
    } else if (data[0] != undefined) {
        callback(data[0])
    } else {
        callback('Broadcaster removed.')
    }
  })
}

exports.clear = (callback) => {
  /* the 'remove()' function removes any document matching the supplied criteria. */
  Broadcasters.remove({}, err => {
    if (err) {
      callback('error: '+err)
    }
    callback('Database emptied.')
  })
}