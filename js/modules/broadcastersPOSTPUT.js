var getTwitch = require('./getTwitch')
var getRiot = require('./getRiot')
var mongo = require('./mongo.js')

// Authorization test.
function authorisationSent(auth) {
    console.log('Checking authentication.')
    return new Promise((resolve, reject) => {
        console.log('  a')
        if (auth.scheme !== 'Basic') {
            console.log('  b')
            console.log('Basic authentication missing.')
            return reject({code: 401, response:{ status:'error', message:'Basic access authentication required.'} })
        }
        if (auth.basic.username === 'davids' || auth.basic.password === 'schillid') {
            console.log('  c')
            console.log('Access granted. Correct username and password.')
            return resolve({code: 200, response:{ status:'success', message:'Access granted.'} })
        }
        console.log('  d')
        return reject({code: 401, response:{ status:'error', message:'Access denied. Wrong username and/or password.'} })
    })
}

// Testing if all data was provided.
function checkBody(body) {
    console.log('Checking body.')
    return new Promise((resolve, reject) => {
        console.log('  a')
        console.log(body)
        if (body == undefined || typeof body.user !== 'string' || typeof body.region !== 'string' || typeof body.summoner !== 'string') {
            console.log('  b')
            return reject({code: 400, response:{status:'error', message:'Twitch username, summoner name and region required.'}})
        }
        console.log('Provided data accepted.')
        return resolve({code: 200, response:{status:'success', message:'Twitch username, summoner name and region present.'}})
    })
}

// Running the TwitchGet module to receive broadcaster data.
function runTwitchGet(user) {
    console.log('Fetching data from Twitch API.')
    return new Promise((resolve, reject) => {
        console.log('  a')
        getTwitch.getStreamData(user, info => {
            console.log('Data retreived, resolving...')
            return resolve({code: 200, response:{status:'success', message:'Twitch data received.'}, data: info})
        })
    })
}

// Running the RiotGet module to receive player data.
function runRiotGet(region, name) {
    console.log('Fetching data from Riot API.')
    return new Promise((resolve, reject) => {
        console.log('  a')
        getRiot.getSummonerData(region, name, info => {
            console.log('Data retreived, resolving...')
            return resolve({code: 200, response:{status:'success', message:'Riot data received.'}, data: info})
        })
    })
}

/* Running the Mongo module in order to store the object to the database. */
function storeBroadcaster(broadcaster) {
    console.log('Storing broadcaster to database.')
    return new Promise((resolve, reject) => {
        console.log('  a')
        mongo.addBroadcaster(broadcaster, data => {
        console.log(data)
        return resolve({code: 200, response:{status:'success', message:'Data stored to database.'}})
        })
    })
}

/* Running the Mongo module in order to update the database entry. */
function updateBroadcaster(broadcaster) {
    console.log('Updating broadcaster entry.')
    return new Promise((resolve, reject) => {
        console.log('  a')
        mongo.updateBroadcaster(broadcaster, data => {
        console.log(data)
        return resolve({code: 200, response:{status:'success', message:'Broadcaster updated.'}})
        })
    })
}

// Broadcaster ADD main promise chain.
exports.add = (auth, body, callback) => {
    var broadcaster = {}
    var twitch, riot
    console.log('---1---')
    authorisationSent(auth)
    .then(() => {
        console.log('---2---')
        return checkBody(body)
  }).then(() => {
        console.log('---3---')
        return runTwitchGet(body.user)
  }).then((data) => {
        console.log('---4---')
        twitch = data
  }).then(() => {
        console.log('---5---')
        return runRiotGet(body.region, body.summoner)
  }).then((data) => {
        console.log('---6---')
        riot = data
  }).then(() => {
        console.log('---7---')
        broadcaster.broadcaster = twitch.data.channel
        broadcaster.summoner = riot.data.summonerName
        broadcaster.stream = twitch.data
        broadcaster.game = riot.data
  }).then(() => {
        console.log('---8---')
        console.log(broadcaster)
        return storeBroadcaster(broadcaster)
  }).then(() => {
        console.log('---9---')
        callback({code:201, response:{status:'success', message:'Broadcaster added.', data: broadcaster}})
  }).catch((data) => {
        console.log('---ERROR---')
        console.log('MAIN CATCH')
        callback(data)
  })
}

// Broadcaster UPDATE main promise chain.
exports.update = (auth, body, callback) => {
    var broadcaster = {}
    var twitch, riot
    console.log('---1---')
    
    authorisationSent(auth)
    .then(() => {
        console.log('---2---')
        return checkBody(body)
  }).then(() => {
        console.log('---3---')
        return runTwitchGet(body.user)
  }).then((data) => {
        console.log('---4---')
        twitch = data
  }).then(() => {
        console.log('---5---')
        return runRiotGet(body.region, body.summoner)
  }).then((data) => {
        console.log('---6---')
        riot = data
  }).then(() => {
        console.log('---7---')
        broadcaster.broadcaster = twitch.data.channel
        broadcaster.summoner = riot.data.summonerName
        broadcaster.stream = twitch.data
        broadcaster.game = riot.data
  }).then(() => {
        console.log('---8---')
        return updateBroadcaster(broadcaster)
  }).then(() => {
        console.log('---9---')
        callback({code:200, response:{status:'success', message:'Broadcaster updated.', data: broadcaster}})
  }).catch((data) => {
        console.log('---ERROR---')
        console.log('MAIN CATCH')
        callback(data)
  })
}