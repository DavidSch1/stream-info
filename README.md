## Twitch stream info.

Resolution,

Frames per second,

Bandwidth usage,

Uptime,

Broadcaster information (Name if available, username, where is he/she streaming from...)

Viewer count,

Follower count,

What champion they're playing as, in the case of League of Legends

...